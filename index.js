import express from 'express';
import cors from 'cors';
import dotenv from 'dotenv';
dotenv.config();
const app=express();
app.use(cors());

const PORT=parseInt(process.env.PORT) || 8002;

app.get('/',(request,response)=>{
    response.status(200).json('Hello Express!');
})
app.listen(PORT,()=>{
    console.log("Server is running on port",PORT);
})
